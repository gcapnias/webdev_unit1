function createBox(row, column){
    var $box = $('<div>');
    $box.addClass('box');
    $box.addClass('box-off');
    
    $box.data({
        'mydata': {
            row: row,
            column: column,
            selected: false
        },
        'george': false
    });
    
    $box.appendTo($('.container'));
    
    return $box;
}

var grid = new Array();

function setBox(row, column, mode){
    if (row < 0 || row > 9){
        return;
    }
    
    if (column < 0 || column > 9){
        return;
    }
    
    var $box = grid[row][column];
    var myData = $box.data('mydata');
    if (mode == 'pen'){
        if (!myData.selected){
            $box.removeClass('box-off');
            $box.addClass('box-on');
            myData.selected = true;
        }
    }
    else if (mode == 'eraser'){
        if (myData.selected){
            $box.removeClass('box-on');
            $box.addClass('box-off');
            myData.selected = false;
        }
    }
    
}


var penSize = 1;
var mode = 'pen';

function onPageReady(){
    for (var r = 0; r < 10; r++) {
        grid[r] = new Array();
        for (var c = 0; c < 10; c++) {
            var $box = createBox(r,c);
            grid[r][c] = $box;
        }
    }
    
    $('#pen-size').on('change', function() {
        var $selected = $('#pen-size').find(':selected');
        penSize = $selected.val();
    })

    $('#mode').on('change', function() {
        var $selected = $('#mode').find(':selected');
        mode = $selected.val();
    })
    
    $('.box').on('click', function(){
        var myData = $(this).data('mydata');
        if (penSize == 1){
            setBox(myData.row, myData.column, mode)
        }
        else if (penSize == 2){
            var setOn = !myData.selected;
            for(var r = -1; r <= 1; r++){
                for(var c = -1; c <= 1; c++){
                    setBox(myData.row+r, myData.column+c, mode);
                }
            }
        }

    });
}

$(document).ready(onPageReady);
