function onButton1Pressed(){
    alert('Button 1 clicked!');
}

function onButton2Pressed(){
    alert('Button 2 clicked!');
}

function onPageReady(){
    $('#button-1').on('click', onButton1Pressed);
    $('#button-2').on('click', onButton2Pressed);
}

//$(document).ready(function(){
//    alert('Hello World from JavaScript!');
//});

$(document).ready(onPageReady);
