WeatherApp.Place = Backbone.Model.extend({
    /*
     Fields:
     country,
     boundingBox,
     centroid,
     name,
     country.content,
     placeTypeName (e.g. Town, Country, etc)
     woeid
     */

    photo: null,
    weatherCondition: null,

    initialize: function () {
        this.set('fullName', this._getFullName());
    },

    _getFullName: function () {
        var type = this.get('placeTypeName');
        var admins = [
            {
                type: type,
                content: this.get('name')
            },
            this.get('admin3'),
            this.get('admin2'),
            this.get('admin1')
        ];

        var fullName = '';
        var lastAdmin = null;
        for (var i in admins) {
            var admin = admins[i];
            if (admin && admin.type != 'Country') {
                if (!lastAdmin || (lastAdmin && lastAdmin.content != admin.content)) {
                    if (fullName.length > 0) {
                        fullName += ', ';
                    }
                    fullName += admin.content;
                    lastAdmin = admin;
                }
            }
        }

        if (type != 'Country') {
            if (fullName.length > 0) {
                fullName += ', ';
            }
            fullName += this.get('country').code;
        }

        return fullName;
    }
});

WeatherApp.findPlaces = function (field, value, callback) {
    var query = 'select * from geo.places where ' + field + '="' + value + '"';
    var places = new WeatherApp.YqlCollection([ ], {
        model: WeatherApp.Place,
        query: query,
        resultField: 'place'
    });

    $('#status').html('Retrieving places...');
    places.fetch({
        error: function (collection, error) {
            callback(error);
        },
        success: function (collection) {
            $('#status').html('Got places...');
            collection.models = _.uniq(collection.models, function(model) {
                return model.get('fullName');
            });
            callback(null, collection);
        }
    });
};

WeatherApp.getCurrentPlace = function (callback) {
    if (navigator.geolocation) {
        $('#status').html('Getting current location from browser...');
        navigator.geolocation.getCurrentPosition(function (position) {
            $('#status').html('Got location...');
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;

            var query = 'select woeid from geo.placefinder where text="' + lat + ',' + lng + '" and gflags="R"';
            var url = '//query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent(query) + '&format=json' +
                '&rnd=' + (new Date().getTime());
            $('#status').html('Asking Yahoo for place...');
            $.getJSON(url, function (data) {
                if (data) {
                    $('#status').html('Yahoo responded with place...');
                    var woeid = data.query.results.Result.woeid;
                    WeatherApp.findPlaces('woeid', woeid, function (err, places) {
                        if (err || !places.at(0)) {
                            callback(err);
                        } else {
                            callback(null, places.at(0));
                        }
                    });
                } else {
                    callback(null, null);
                }
            });
            console.log('next!');
        });
    }
};

WeatherApp.getWeatherForecast = function(woeid, callback) {
    var query = 'select * from weather.forecast where woeid=' + woeid + ' AND u="c"';
    var url = '//query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent(query) + '&format=json' +
    '&rnd=' + (new Date().getTime());
    $('#status').html('Asking Yahoo for weather forecast...');
    $.getJSON(url, function (data) {
        if (data) {
            $('#status').html('Yahoo responded with wether forecast...');
            var description = data.query.results.channel.item.description;
            callback(null, description);
        } else {
            callback(null, null);
        }
    });
};
