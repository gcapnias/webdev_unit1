WeatherApp.onPageReady = function() {
    $('#status').html('Started!');
    
    $('#press').on('click', function() {
        alert('Yeah!');    
    });
    
    WeatherApp.getCurrentPlace(function(err, place) {
        if (err) {
            $('#place').html('an error occurred: ' + err.message);
            console.log(err);
        } else {
            //$('#place').html('got place ' + place.get('fullName') + ' with woeid ' + place.get('woeid'));
            //console.log(place.toJSON());
            var woeid = place.get('woeid');
            
            WeatherApp.getWeatherForecast(woeid, function(err, description){
                if (description)
                    $('#temperature').html(description);
            })
            
            console.log(woeid);
        }
    })
}

$(document).ready(WeatherApp.onPageReady);