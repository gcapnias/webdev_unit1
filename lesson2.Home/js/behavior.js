function createBox(row, column){
    var $box = $('<div>');
    $box.addClass('box');
    $box.addClass('box-off');
    
    $box.data({
        'mydata': {
            row: row,
            column: column,
            selected: false
        },
        'george': false
    });
    
    $box.appendTo($('.container'));
    
    return $box;
}

var grid = new Array();

function setBox(row, column, mode, color){
    if (row < 0 || row > 19){
        return;
    }
    
    if (column < 0 || column > 19){
        return;
    }
    
    var $box = grid[row][column];
    var myData = $box.data('mydata');
    if (mode == 'pen'){
        if (!myData.selected){
            $box.removeClass('box-off');
            $box.addClass('box-on-' + color);
            myData.selected = true;
        }
    }
    else if (mode == 'eraser'){
        if (myData.selected){
            if (eraseColor){
                switch (color) {
                    case "red":
                        if ($box.hasClass('box-on-red')){
                            $box.removeClass('box-on-red');
                            $box.addClass('box-off');
                            myData.selected = false;
                        }
                        break;
                        
                    case "green":
                        if ($box.hasClass('box-on-green')){
                            $box.removeClass('box-on-green');
                            $box.addClass('box-off');
                            myData.selected = false;
                        }
                        break;
                        
                    case "yellow":
                        if ($box.hasClass('box-on-yellow')){
                            $box.removeClass('box-on-yellow');
                            $box.addClass('box-off');
                            myData.selected = false;
                        }
                        break;
                        
                    default:
                        break;
                }
            }
            else 
            {
                $box.removeClass('box-on-red');
                $box.removeClass('box-on-green');
                $box.removeClass('box-on-yellow');
                $box.addClass('box-off');
                myData.selected = false;
            }
        }
    }
    
}


var penSize = 1;
var mode = 'pen';
var color = 'red';
var eraseColor = false;

function onPageReady(){
    for (var r = 0; r < 20; r++) {
        grid[r] = new Array();
        for (var c = 0; c < 20; c++) {
            var $box = createBox(r,c);
            grid[r][c] = $box;
        }
    }
    
    $('#pen-size').on('change', function() {
        var $selected = $('#pen-size').find(':selected');
        penSize = $selected.val();
    })

    $('#mode').on('change', function() {
        var $selected = $('#mode').find(':selected');
        mode = $selected.val();
    })

    $('#color').on('change', function() {
        var $selected = $('#color').find(':selected');
        color = $selected.val();
    })
    
    $('#eraseColor').click(function() {
    if ($(this).is(':checked')) {
        eraseColor = true;
    } else {
        eraseColor = false;
    }
});
    
    $('.box').on('click', function(){
        var myData = $(this).data('mydata');
        if (penSize == 1){
            setBox(myData.row, myData.column, mode, color)
        }
        else if (penSize == 2){
            var setOn = !myData.selected;
            for(var r = -1; r <= 1; r++){
                for(var c = -1; c <= 1; c++){
                    setBox(myData.row+r, myData.column+c, mode, color);
                }
            }
        }

    });
}

$(document).ready(onPageReady);
