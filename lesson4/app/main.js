
WeatherApp.lastPlace = null;
WeatherApp.currentPlace = null;

WeatherApp.setBackgroundPhoto = function (error, photo) {
    if (error) {
        $('.backgroundImage').css('background-image', 'none');
    } else {
        $('.backgroundImage').css('background-image', 'url(' + photo.get('photo_file_url') + ')');
    }
};

WeatherApp.setWeatherCondition = function (error, weatherCondition) {
    WeatherApp.currentView.model = weatherCondition;
    WeatherApp.currentView.render().$el.fadeIn();
};

WeatherApp.setPlace = function (place) {
    if (!place) {
        return;
    }

    WeatherApp.lastPlace = WeatherApp.currentPlace;
    WeatherApp.currentPlace = place;

    place.getPhoto(WeatherApp.setBackgroundPhoto);
    place.getWeatherCondition('c', WeatherApp.setWeatherCondition);
};


WeatherApp.onPageReady = function () {
    new Spinner({
        className: 'spinner',
        top: 160
    }).spin($('.weather-container')[0]);
    $('.spinner').fadeIn();

    WeatherApp.currentView = new WeatherApp.CurrentView();

    WeatherApp.getCurrentPlace(function (err, place) {
        if (err) {
            $('#place').html('an error occurred: ' + err.message);
            console.log(err);
        } else {
            $('#place').html('got place ' + place.get('fullName') + ' with woeid ' + place.get('woeid'));
            console.log(place.toJSON());
            var woeid = place.get('woeid');
            console.log(woeid);

            WeatherApp.setPlace(place);
        }
        $('.spinner').fadeOut();
    })
};

$(document).ready(WeatherApp.onPageReady);