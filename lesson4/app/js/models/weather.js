WeatherApp.WeatherCondition = Backbone.Model.extend({

    _getWindDirectionSymbol: function (wd) {
        if (wd >= 348.75 && wd <= 360) {
            return "N";
        } else if (wd >= 0 && wd < 11.25) {
            return "N";
        } else if (wd >= 11.25 && wd < 33.75) {
            return "NNE";
        } else if (wd >= 33.75 && wd < 56.25) {
            return "NE";
        } else if (wd >= 56.25 && wd < 78.75) {
            return "ENE";
        } else if (wd >= 78.75 && wd < 101.25) {
            return "E";
        } else if (wd >= 101.25 && wd < 123.75) {
            return "ESE";
        } else if (wd >= 123.75 && wd < 146.25) {
            return "SE";
        } else if (wd >= 146.25 && wd < 168.75) {
            return "SSE";
        } else if (wd >= 168.75 && wd < 191.25) {
            return "S";
        } else if (wd >= 191.25 && wd < 213.75) {
            return "SSW";
        } else if (wd >= 213.75 && wd < 236.25) {
            return "SW";
        } else if (wd >= 236.25 && wd < 258.75) {
            return "WSW";
        } else if (wd >= 258.75 && wd < 281.25) {
            return "W";
        } else if (wd >= 281.25 && wd < 303.75) {
            return "WNW";
        } else if (wd >= 303.75 && wd < 326.25) {
            return "NW";
        } else if (wd >= 326.25 && wd < 348.75) {
            return "NNW";
        }
    },

    _getTimeAsDate: function (t) {
        var d = new Date();
        return new Date(d.toDateString() + ' ' + t);
    },

    _isDayTime: function (dateString, sunriseTime, sunsetTime) {
        var pos = dateString.indexOf(":");
        var date = this._getTimeAsDate(dateString.substr(pos - 2, 8));
        var sunriseDate = this._getTimeAsDate(sunriseTime);
        var sunsetDate = this._getTimeAsDate(sunsetTime);

        return (date > sunriseDate && date < sunsetDate);
    },

    _extractForecast: function (forecast, isDayTime) {
        var date = forecast.date;
        var pos = date.lastIndexOf(' ');
        var dateWithoutYear = date.substring(0, pos);
        return {
            icon: 'http://l.yimg.com/a/i/us/nws/weather/gr/' + forecast.code + (isDayTime ? 'd' : 'n') + '.png',
            day: forecast.day,
            date: dateWithoutYear,
            text: forecast.text,
            high: forecast.high,
            low: forecast.low
        }
    },

    _getForecasts: function (data) {
        var orgForecasts = data.item.forecast;
        var count = orgForecasts.length;
        var forecasts = new Array(count - 1);
        for (var i = 1; i < count; i++) {
            forecasts[i - 1] = this._extractForecast(orgForecasts[i], true);
        }
        return forecasts;
    },

    parse: function (response, options) {
        var isDayTime = this._isDayTime(response.item.pubDate, response.astronomy.sunrise, response.astronomy.sunset);
        return {
            description: response.description,
            wind: {
                chill: response.wind.chill,
                direction: this._getWindDirectionSymbol(response.wind.direction),
                speed: response.wind.speed + response.units.speed
            },
            isDayTime: isDayTime,
            temperature: response.item.condition.temp,
            condition: response.item.condition.text,
            humidity: response.atmosphere.humidity,
            visibility: response.atmosphere.visibility,
            sunrise: response.astronomy.sunrise,
            sunset: response.astronomy.sunset,
            current: this._extractForecast(response.item.forecast[0], isDayTime),
            forecasts: this._getForecasts(response),
            link: response.link
        }
    }
});

WeatherApp.getWeatherConditions = function (places, unit, callback) {
    if (!$.isArray(places)) {
        places = [places];
    }
    var locationIDs = '';
    for (var i in places) {
        if (locationIDs != '') {
            locationIDs += ',';
        }
        locationIDs += places[i].get('woeid');
    }
    var query = "select * from weather.forecast where woeid in (" + locationIDs + ") and u='" + unit + "'";
    var weatherConditions = new WeatherApp.YqlCollection([], {
        model: WeatherApp.WeatherCondition,
        query: query,
        resultField: 'channel'
    });

    weatherConditions.fetch({
        error: function (collection, error) {
            callback(error);
        },
        success: function (collection) {
            callback(null, collection);
        }
    });
};
