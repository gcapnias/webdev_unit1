WeatherApp.PanoramioPhoto = Backbone.Model.extend({

    /*
     Fields:
     photo_file_url
     photo_title
     width
     height
     owner_name
     owner_url
     */
});

WeatherApp.PanoramioPhotoCollection = Backbone.Collection.extend({
    boundingBox: null,

    model: WeatherApp.PanoramioPhoto,
    url: 'https://ssl.panoramio.com/map/get_panoramas.php',
    format: 'json',
    timeout: 10000,

    initialize: function (models, options) {
        this.boundingBox = options.boundingBox;
    },

    fetch: function (options) {
        var southWest = this.boundingBox.southWest;
        var northEast = this.boundingBox.northEast;
        options = _.extend({
            data: {
                set: 'public',
                from: 0,
                to: 20,
                minx: southWest.longitude,
                miny: southWest.latitude,
                maxx: northEast.longitude,
                maxy: northEast.latitude,
                size: 'medium'
            }
        }, options);
        return Backbone.Collection.prototype.fetch.call(this, options);
    },

    sync: function (method, model, options) {
        options.timeout = _.result(this, 'timeout');
        options.dataType = 'jsonp';
        return Backbone.sync(method, model, options);
    },

    parse: function (response, options) {
        return response.photos;
    }
});
