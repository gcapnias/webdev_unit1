WeatherApp.Place = Backbone.Model.extend({
    /*
     Fields:
     country,
     boundingBox,
     centroid,
     name,
     country.content,
     placeTypeName (e.g. Town, Country, etc)
     woeid
     */

    photo: null,
    weatherCondition: null,

    initialize: function () {
        this.set('fullName', this._getFullName());
    },

    _getFullName: function () {
        var type = this.get('placeTypeName');
        var admins = [
            {
                type: type,
                content: this.get('name')
            },
            this.get('admin3'),
            this.get('admin2'),
            this.get('admin1')
        ];

        var fullName = '';
        var lastAdmin = null;
        for (var i in admins) {
            var admin = admins[i];
            if (admin && admin.type != 'Country') {
                if (!lastAdmin || (lastAdmin && lastAdmin.content != admin.content)) {
                    if (fullName.length > 0) {
                        fullName += ', ';
                    }
                    fullName += admin.content;
                    lastAdmin = admin;
                }
            }
        }

        if (type != 'Country') {
            if (fullName.length > 0) {
                fullName += ', ';
            }
            fullName += this.get('country').code;
        }

        return fullName;
    },

    getPhoto: function (callback) {
        if (!this.photo) {
            var photos = new WeatherApp.PanoramioPhotoCollection([], {
                boundingBox: this.get('boundingBox')
            });

            var me = this;
            photos.fetch({
                success: function (collection) {
                    var count = collection.models.length;
                    var index = Math.floor(Math.random() * count);
                    me.photo = collection.at(index);
                    callback(null, me.photo);
                }
            })
        } else {
            callback(null, this.photo);
        }
    },

    getWeatherCondition: function (unit, callback) {
        if (!this.weatherCondition) {
            var me = this;
            WeatherApp.getWeatherConditions(this, unit, function (error, weatherConditions) {
                var weatherCondition = (error) ? false : weatherConditions.at(0);
                if (!error) {
                    me.weatherCondition = weatherCondition
                }
                callback(error, weatherCondition);
            });
        } else {
            callback(null, this.weatherCondition);
        }
    }
});

WeatherApp.findPlaces = function (field, value, callback) {
    var query = 'select * from geo.places where ' + field + '="' + value + '"';
    var places = new WeatherApp.YqlCollection([], {
        model: WeatherApp.Place,
        query: query,
        resultField: 'place'
    });

    places.fetch({
        error: function (collection, error) {
            callback(error);
        },
        success: function (collection) {
            collection.models = _.uniq(collection.models, function (model) {
                return model.get('fullName');
            });
            callback(null, collection);
        }
    });
};

WeatherApp.getCurrentPlace = function (callback) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;

            var query = 'select woeid from geo.placefinder where text="' + lat + ',' + lng + '" and gflags="R"';
            var url = '//query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent(query) + '&format=json&rnd=' + (new Date().getTime());
            $.getJSON(url, function (data) {
                if (data) {
                    var woeid = data.query.results.Result.woeid;
                    WeatherApp.findPlaces('woeid', woeid, function (err, places) {
                        if (err || !places.at(0)) {
                            callback(err);
                        } else {
                            callback(null, places.at(0));
                        }
                    });
                } else {
                    callback(null, null);
                }
            });
        });
    }
};