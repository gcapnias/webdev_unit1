WeatherApp.YqlCollection = Backbone.Collection.extend({
    query: '',
    resultField: '',

    url: '//query.yahooapis.com/v1/public/yql',
    format: 'json',
    timeout: 10000,

    initialize: function (models, options) {
        this.query = options.query;
        this.resultField = options.resultField;
    },

    setQuery: function (query) {
        this.query = query;
    },

    fetch: function (options) {
        options = _.extend({
            data: {
                q: _.result(this, 'query'),
                format: 'json',
                rnd: (new Date().getTime())
            }
        }, options);
        return Backbone.Collection.prototype.fetch.call(this, options);
    },

    sync: function (method, model, options) {
        options.timeout = _.result(this, 'timeout');
        options.dataType = 'jsonp';
        return Backbone.sync(method, model, options);
    },

    parse: function (response, options) {
        var resultField = _.result(this, 'resultField');
        if (response.error) {
            options.error(response.error);
            return false;
        } else {
            var results = response.query.results;
            return results ? results[resultField] : [ ];
        }
    }
});
