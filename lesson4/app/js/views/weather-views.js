WeatherApp.CurrentView = Backbone.View.extend({
    el: '.weather-panel.current',

    initialize: function () {
        this._template = _.template($('#CurrentTemplate').html());
    },

    render: function () {
        var data = this.model.toJSON();
        var html = this._template(data);
        this.$el.html(html);
        return this;
    }
});

WeatherApp.ForecastView = Backbone.View.extend({
    // TODO Assignment
});